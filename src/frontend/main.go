package main

import (
	"fmt"
	"net/http"
	"html/template"
)

type FrameDot struct {
	SiteName, SiteRoot, UserName, UserAvatar, UserHref string
	UserLoggedIn bool
}

type HomeDot struct {
	FrameDot
	Recommended []Recommendation
}

type ProfileDot struct {
	FrameDot
	ProfileName, ProfileAvatar, ProfileHref, ProfileHeader, ProfileDescription string
	ProfileTracks, Recommended []Recommendation
}

type TrackDot struct {
	FrameDot
	TrackTitle, TrackAuthorName, TrackAuthorAvatar, TrackAuthorHref, TrackCover, TrackAudio, TrackDescription string
	Comments []Comment
	MoreFrom, Recommended []Recommendation
}

type HTMLOutput []byte

func (this *HTMLOutput) Write(toWrite []byte) (int, error) {
	*this = append(*this, toWrite...)
	return len(toWrite), nil
}

type Comment struct {
	Name, Avatar, Href, Content string
	Date int64
}

type Recommendation struct {
	Cover, Href string
}

var myHomeDot = HomeDot{
	FrameDot: FrameDot{
		SiteName: "Project ClodGlass",
		SiteRoot: "/",
		UserName: "tromino",
		UserAvatar: "/assets/mockup-media/avatar.png",
		UserHref: "/u/tromino",
		UserLoggedIn: true,
	},
	Recommended: []Recommendation{
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
	},
}

var myProfileDot = ProfileDot{
	FrameDot: FrameDot{
		SiteName: "Project ClodGlass",
		SiteRoot: "/",
		UserName: "tromino",
		UserAvatar: "/assets/mockup-media/avatar.png",
		UserHref: "/u/tromino",
		UserLoggedIn: true,
	},
	ProfileName: "tromino",
	ProfileAvatar: "/assets/mockup-media/avatar.png",
	ProfileHref: "/u/tromino",
	ProfileHeader: "/assets/mockup-media/avatar.png",
	ProfileDescription: "ohaithar",
	ProfileTracks: []Recommendation{
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
	},
	Recommended: []Recommendation{
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
	},
}

var myTrackDot = TrackDot{
	FrameDot: FrameDot{
		SiteName: "Project ClodGlass",
		SiteRoot: "/",
		UserName: "tromino",
		UserAvatar: "/assets/mockup-media/avatar.png",
		UserHref: "/u/tromino",
		UserLoggedIn: true,
	},
	TrackTitle: "Intuitive Thoughts",
	TrackAuthorName: "tromino",
	TrackAuthorAvatar: "/assets/mockup-media/avatar.png",
	TrackAuthorHref: "/u/tromino",
	TrackCover: "/assets/mockup-media/avatar.png",
	TrackAudio: "/assets/mockup-media/music.ogg",
	TrackDescription: "wonder if <b>html</b> is escaped correctly... also hi!!",
	Comments: []Comment{
		{
			Name: "tromino",
			Avatar: "/assets/mockup-media/avatar.png",
			Href: "/u/tromino",
			Content: "testing, testing",
			Date: 42318705,
		},
		{
			Name: "tromino",
			Avatar: "/assets/mockup-media/avatar.png",
			Href: "/u/tromino",
			Content: "hmmm...",
			Date: 42318261,
		},
	},
	MoreFrom: []Recommendation{
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
	},
	Recommended: []Recommendation{
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
		{
			Cover: "/assets/mockup-media/avatar.png",
			Href: "/t/example",
		},
	},
}

func main() {
	http.Handle(
		myTrackDot.SiteRoot + "assets/",
		http.StripPrefix(
			myTrackDot.SiteRoot + "assets/",
			http.FileServer(http.Dir("assets")),
		),
	)
	
	http.HandleFunc(myTrackDot.SiteRoot, func(
		writer http.ResponseWriter, req *http.Request, // TODO: Use global site root var
	) {
		outputTemplate, err := template.New("frame.html").ParseFiles("html/frame.html", "html/home.html")
		
		if err == nil {
			var outputHTML HTMLOutput = make([]byte, 0)
			err = outputTemplate.Execute(&outputHTML, myHomeDot)
			
			if err == nil {
				writer.Write(outputHTML)
			} else {
				fmt.Println(err)
			}
		} else {
			fmt.Println(err)
		}
	})
	
	http.HandleFunc(myTrackDot.SiteRoot + "u/", func(
		writer http.ResponseWriter, req *http.Request,
	) {
		outputTemplate, err := template.New("frame.html").ParseFiles("html/frame.html", "html/profile.html")
		
		if err == nil {
			var outputHTML HTMLOutput = make([]byte, 0)
			err = outputTemplate.Execute(&outputHTML, myProfileDot)
			
			if err == nil {
				writer.Write(outputHTML)
			} else {
				fmt.Println(err)
			}
		} else {
			fmt.Println(err)
		}
	})
	
	http.HandleFunc(myTrackDot.SiteRoot + "t/", func(
		writer http.ResponseWriter, req *http.Request,
	) {
		outputTemplate, err := template.New("frame.html").ParseFiles("html/frame.html", "html/track.html")
		
		if err == nil {
			var outputHTML HTMLOutput = make([]byte, 0)
			err = outputTemplate.Execute(&outputHTML, myTrackDot)
			
			if err == nil {
				writer.Write(outputHTML)
			} else {
				fmt.Println(err)
			}
		} else {
			fmt.Println(err)
		}
	})
	
	fmt.Println("Running frontend server on port 8830")
	http.ListenAndServe(":8830", nil)
}
